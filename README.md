# Globenewswire's Scraper

Simple scraper for [globenewswire.com](http://globenewswire.com). If you will, I can also make more advance verion(s).

Note: globenewswire.com somewhat tricky, so it's not the best way to learn scraping. For more details, see `globenewswire_com_beginner/spiders/search.py`.

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)
* [scrapy](https://doc.scrapy.org/en/latest/intro/install.html)
* [shub](https://helpdesk.scrapinghub.com/support/solutions/articles/22000204081-deploying-your-spiders-to-scrapy-cloud) (optionally, for deployment to scrapinghub.com)

### Installing

Clone project and change directory:

```
git clone https://nsalikov@bitbucket.org/nsalikov/globenewswire_com_beginner.git
cd globenewswire_com_beginner
```

### Configuration

Not required.

### Usage

Just run:

```
scrapy crawl search
```

To save output you need to specify command-line options:

```
# feed export to json, extensions matter
scrapy crawl search -o marijuana.json

# feed export to json-lines
scrapy crawl search -o marijuana.jl

# feed export to csv
scrapy crawl search -o marijuana.csv

# to save the log as well (options are in the GNU style):
scrapy crawl search --output=marijuana.json --logfile=marijuana.log
```

To enable [persistence support](https://doc.scrapy.org/en/latest/topics/jobs.html) you need to define job directory:

```
scrapy crawl search --output=marijuana.json --logfile=marijuana.log -s JOBDIR=globenewswire_com_beginner/job
```

Hope this helps.