# -*- coding: utf-8 -*-
import re
import scrapy
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class SearchSpider(scrapy.Spider):
    name = 'search'
    allowed_domains = ['globenewswire.com']
    start_urls = ['http://globenewswire.com/']

    keywords = ['marijuana']
    ticker_pattern = re.compile(r'[ (]([^\(\)\:\"]{1,12}\:[^\(\)\:\"]{1,12})[;,)]')


    def parse(self, response):
        # In order to use search, you should get cookie first.
        # So, we send requests through scrapy.FormRequest.from_response
        # https://doc.scrapy.org/en/latest/topics/request-response.html#formrequest-objects

        formdata = {
                'quicksearch-textbox': 'test',
        }

        return scrapy.FormRequest.from_response(response,
                                                formid='layout_search_form',
                                                formdata=formdata,
                                                method='POST',
                                                callback=self.do_search,
                                                dont_filter=True)


    def do_search(self, response):
        # In addition to cookie, globenewswire.com uses hidden field to prevents bot using.
        # We must therefore use scrapy.FormRequest.from_response again.
        # Note different callback.
        # ALso note dont_filter=True. It would be helpful, if you'll be using persistence support
        # https://doc.scrapy.org/en/latest/topics/jobs.html

        for keyword in self.keywords:
            formdata = {
                'keywords': keyword,
            }

            yield scrapy.FormRequest.from_response( response,
                                                    formid='layout_search_form',
                                                    formdata=formdata,
                                                    method='POST',
                                                    callback=self.handle_pagination,
                                                    dont_filter=True)


    def handle_pagination(self, response):
        # Pagination's requests generation.
        # Note dont_filter=True in requests.

        max_page = 1
        pages = [re.sub('\s*changePage\((\d+)\).*', '\g<1>', t) for t in response.css('ul.ui-html-pager li a ::attr(onclick)').extract() if 'changePage' in t]

        for page in pages:
            try:
                page = int(page)
                if page > max_page:
                    max_page = page
            except:
                continue

        if max_page == 1:
            return

        for page in range(2, max_page + 1):
            # Will be redirected to to:
            # http://globenewswire.com/Search/NewsSearch?keyword=marijuana&page=PAGE

            formdata = {
                'PageIndex': str(page),
            }

            yield scrapy.FormRequest.from_response( response,
                                                    formid='main_form',
                                                    formdata=formdata,
                                                    method='POST',
                                                    callback=self.parse_search,
                                                    dont_filter=True)


    def parse_search(self, response):
        # Simple parsing of search results.

        companies = response.css('div.results-link h1 a ::attr(href)').extract()
        for url in companies:
            yield response.follow(url=url, callback=self.parse_company)


    def parse_company(self, response):
        # Parsing companies' profiles.

        url = response.url
        item_id = re.sub('.*?news-release/\d\d\d\d/\d\d/\d\d/(\d+)/.*', '\g<1>', url)
        title = response.xpath('/html/body/div[2]/h1/text()').extract_first()
        date = response.xpath('/html/body/div[2]/div[1]/div[2]/span[1]/span/em/time/text()').extract_first()
        content = ''.join(response.xpath('//*[@id="content-L2"]/descendant::*/text()').extract())
        content = [re.sub('\s+', '', c) for c in re.findall(self.ticker_pattern, content)]

        d = {
                'id': item_id,
                'date': date,
                'title': title,
                'content': content,
                'url': url,
        }

        # In case if something goes wrong.
        # inspect_response(response, self)

        return d
